package stackunderflow.util.commandfactory;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Map;


/*
 * A wrapper for some command logic.
 * Handles stateless & stateful logic.
 */
@Getter
@Setter
public class Command {


    // ================================     VARS


    // References

    // Settings
    private String cmdString;






    // ================================     CONSTRUCTOR


    public Command(String cmdString) {

        setCmdString(cmdString);

    }



    // ================================     COMMAND FACTORY LOGIC


    public void onPlayerCall(Player sender, Map<String, CommandArg> args) {}
    public void onConsoleCall(CommandSender sender, Map<String, CommandArg> args) {}


}
