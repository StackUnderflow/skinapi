package stackunderflow.skinapi.commands;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import stackunderflow.skinapi.SkinAPIPlugin;
import stackunderflow.util.StringFormatter;
import stackunderflow.util.commandfactory.Command;
import stackunderflow.util.commandfactory.CommandArg;

import java.util.Map;


/*
 * A basic command to set the players skin to another players skin.
 * It is disabled by default but can be enable through the config.yml
 */
@Getter
@Setter
public class CommandSkinApi extends Command {

    // ================================     VARS

    // Null



    // ================================     CONSTRUCTOR


    public CommandSkinApi(String cmdString) {
        super(cmdString);
    }



    // ================================     COMMAND LOGIC


    @Override
    public void onPlayerCall(Player sender, Map<String, CommandArg> args) {

        System.out.println("Commadn triggered");
        for (Map.Entry<String, CommandArg> entry : args.entrySet())
        {
            System.out.println(entry.getKey());
            System.out.println(entry.getValue().getValue());
            System.out.println("==");
        }

        // Ret: !permission
        if (!sender.hasPermission("skinapi.setskin")) return;

        // Ret: Target Player skin name not given.
        //if (!args.get("player").isGiven()) return;

        // Get target player.
        Player targetPlayer = Bukkit.getPlayer(args.get("player").getValue());

        // Ret: !playerExists
        if (targetPlayer == null) return;

        // Get the skin uniqueID.
        OfflinePlayer skinOwnerPlayer = Bukkit.getOfflinePlayer(args.get("skinName").getValue());

        // Ret: !playerExists
        if (skinOwnerPlayer == null) return;

        // Change the players skin.
        SkinAPIPlugin.INSTANCE.getAPI().changePlayerSkin(targetPlayer, skinOwnerPlayer.getUniqueId());

    }


    @Override
    public void onConsoleCall(CommandSender sender, Map<String, CommandArg> args) {
        new StringFormatter("§cThis command can only be executed by players!")
                .sendMessageTo(sender);
    }

}
