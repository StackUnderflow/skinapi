package stackunderflow.skinapi.api;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.EnumWrappers;
import com.comphenix.protocol.wrappers.PlayerInfoData;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import com.comphenix.protocol.wrappers.WrappedGameProfile;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.PlayerInventory;
import stackunderflow.skinapi.caching.Cache;
import stackunderflow.skinapi.database.DatabaseActions;
import stackunderflow.skinapi.playerutils.GameProfileFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

import static com.comphenix.protocol.PacketType.Play.Server.PLAYER_INFO;
import static com.comphenix.protocol.PacketType.Play.Server.POSITION;
import static com.comphenix.protocol.PacketType.Play.Server.RESPAWN;


/*
 * Holds a skins data / methods to apply the skin etc.
 */
@Getter
@Setter
public class SkinData {

    // ================================     VARS

    // References
    public UUID skinOwnerUUID;

    private String profileDataString = "{}";

    // State






    // ================================     CONSTRUCTOR

    public SkinData() {}
    public SkinData(UUID skinOwnerUUID) {

        setSkinOwnerUUID(skinOwnerUUID);

    }
    public SkinData(UUID skinOwnerUUID, String profileDataString) {

        setSkinOwnerUUID(skinOwnerUUID);
        setProfileDataString(profileDataString);

    }


    /*
     * Gets the PlayerData from the db or creates a new one.
     */
    public static SkinData get(UUID uuid) {

        // Get from db.
        SkinData sData = DatabaseActions.INSTANCE.getSkinData(uuid);
        if (sData != null) {

            // Cache.
            Cache.INSTANCE.addSkinData(sData);

            return sData;
        }

        // Create new one.
        sData = new SkinData(uuid);
        sData.fetchProfileData();
        sData.save();

        // Cache.
        Cache.INSTANCE.addSkinData(sData);

        return sData;

    }


    // ================================     SKIN DATA LOGIC


    /*
     * TODO: REWRITE
     */
    private WrappedGameProfile getWrappedGameProfile(CraftPlayer target) {

        GameProfile gameProfile = GameProfileFactory.INSTANCE.buildProfile(getProfileDataString());

        // Reset everything except the textures.
        Collection<Property> props = gameProfile.getProperties().get("textures");
        target.getProfile().getProperties().clear();
        target.getProfile().getProperties().putAll("texture", props);

        WrappedGameProfile wrapped = WrappedGameProfile.fromPlayer(target);
        return wrapped;

    }
    public void applyToPlayer(Player player) {

        Optional.ofNullable(player.getVehicle()).ifPresent(Entity::eject);

        // Send the needed packets.
        sendPacketsSelf(getWrappedGameProfile((CraftPlayer) player), (CraftPlayer) player);

        // Set player stuff so that this stays the same.
        player.setExp(player.getExp());
        player.setWalkSpeed(player.getWalkSpeed());
        player.updateInventory();
        PlayerInventory inventory = player.getInventory();
        inventory.setHeldItemSlot(inventory.getHeldItemSlot());

        // Trigger update attributes like health modifier for generic.maxHealth
        try {
            player.getClass().getDeclaredMethod("updateScaledHealth").invoke(player);
        } catch (ReflectiveOperationException reflectiveEx) {
            reflectiveEx.printStackTrace();
        }

        for (Player pp : Bukkit.getOnlinePlayers()) {
            pp.hidePlayer(player);
            pp.showPlayer(player);
        }


    }
    public void sendPacketsSelf(WrappedGameProfile gameProfile, CraftPlayer p) {
        EnumWrappers.NativeGameMode gamemode = EnumWrappers.NativeGameMode.fromBukkit(p.getGameMode());
        WrappedChatComponent displayName = WrappedChatComponent.fromText(p.getPlayerListName());
        PlayerInfoData playerInfoData = new PlayerInfoData(gameProfile, 0, gamemode, displayName);

        // Remove the old skin - client updates it only on a complete remove and add.
        PacketContainer removeInfo = new PacketContainer(PLAYER_INFO);
        removeInfo.getPlayerInfoAction().write(0, EnumWrappers.PlayerInfoAction.REMOVE_PLAYER);
        removeInfo.getPlayerInfoDataLists().write(0, Collections.singletonList(playerInfoData));

        // Add info containing the skin data.
        PacketContainer addInfo = removeInfo.deepClone();
        addInfo.getPlayerInfoAction().write(0, EnumWrappers.PlayerInfoAction.ADD_PLAYER);

        // Respawn packet - notify the client that it should update the own skin.
        EnumWrappers.Difficulty difficulty = EnumWrappers.getDifficultyConverter().getSpecific(p.getWorld().getDifficulty());

        PacketContainer respawn = new PacketContainer(RESPAWN);
        respawn.getIntegers().write(0, p.getWorld().getEnvironment().getId());
        respawn.getDifficulties().write(0, difficulty);
        respawn.getGameModes().write(0, gamemode);
        respawn.getWorldTypeModifier().write(0, p.getWorld().getWorldType());

        Location location = p.getLocation().clone();

        // Prevent the moved too quickly message.
        PacketContainer teleport = new PacketContainer(POSITION);
        teleport.getModifier().writeDefaults();
        teleport.getDoubles().write(0, location.getX());
        teleport.getDoubles().write(1, location.getY());
        teleport.getDoubles().write(2, location.getZ());
        teleport.getFloat().write(0, location.getYaw());
        teleport.getFloat().write(1, location.getPitch());
        // Send an invalid teleport id in order to let Bukkit ignore the incoming confirm packet.
        teleport.getIntegers().writeSafely(0, -1337);

        sendPackets(p, removeInfo, addInfo, respawn, teleport);
    }
    private void sendPackets(CraftPlayer p, PacketContainer... packets) {
        try {
            ProtocolManager protocolManager = ProtocolLibrary.getProtocolManager();
            for (PacketContainer packet : packets) {
                protocolManager.sendServerPacket(p, packet);
            }
        } catch (InvocationTargetException ex) {
            ex.printStackTrace();
        }
    }


    /*
     * Updates the instance & the db entry with new profile data from
     * Mojang servers if no error happens.
     */
    public void fetchProfileData() {

        String newProfileData = GameProfileFactory.INSTANCE.fetchData(getSkinOwnerUUID());

        // Ret: Error -> Keep data.
        if (newProfileData == "") return;

        // Update & save.
        setProfileDataString(newProfileData);
        save();

    }


    /*
     * Saves the skinData to the database
     * or updates an existing entry.
     */
    public void save() {
        DatabaseActions.INSTANCE.saveSkinData(this);
    }

}
