package stackunderflow.skinapi.api;


import lombok.Getter;
import lombok.Setter;
import stackunderflow.skinapi.caching.Cache;
import stackunderflow.skinapi.database.DatabaseActions;
import stackunderflow.skinapi.playerutils.UUIDUtil;

import java.util.UUID;


/*
 * Holds a players data / methods to apply get the skin object etc.
 */
@Getter
@Setter
public class PlayerData {

    // ================================     VARS

    // References
    public UUID playerUUID;

    // Settings
    private String userName;
    private UUID currentSkinUUID;





    // ================================     CONSTRUCTOR

    public PlayerData() {}
    public PlayerData(UUID uuid) {

        setPlayerUUID(uuid);

    }
    public PlayerData(UUID uuid, String userName, UUID currentSkinUUID) {

        setPlayerUUID(uuid);
        setUserName(userName);
        setCurrentSkinUUID(currentSkinUUID);

    }


    /*
     * Gets the PlayerData from the db or creates a new one.
     */
    public static PlayerData get(UUID uuid) {

        // Get from db.
        PlayerData pData = DatabaseActions.INSTANCE.getPlayerData(uuid);
        if (pData != null) {

            // Cache.
            Cache.INSTANCE.addPlayerData(pData);

            return pData;

        }

        // Create new one.
        pData = new PlayerData(uuid, UUIDUtil.getName(uuid), uuid);
        pData.save();

        // Cache.
        Cache.INSTANCE.addPlayerData(pData);

        return pData;

    }



    // ================================     PLAYER DATA LOGIC


    /*
     * Saves the playerData to the database
     * or updates an existing entry.
     */
    public void save() {
        DatabaseActions.INSTANCE.savePlayerData(this);
    }

}
