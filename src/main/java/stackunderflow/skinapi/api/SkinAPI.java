package stackunderflow.skinapi.api;

import com.mojang.authlib.GameProfile;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import stackunderflow.skinapi.caching.Cache;
import stackunderflow.skinapi.database.DatabaseActions;
import stackunderflow.skinapi.events.PlayerSkinChangeEvent;
import stackunderflow.skinapi.playerutils.GameProfileFactory;
import stackunderflow.skinapi.playerutils.UUIDUtil;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


/*
 * The main SkinAPI call you need to access all the API functionality.
 */
@Getter
@Setter
public class SkinAPI {

    // ================================     VARS

    // References



    // ================================     CONSTRUCTOR

    public SkinAPI() {

        // Setup managers & API Reference.
        new GameProfileFactory();
        new Cache();

    }



    // ================================     DATA API

    /*
     * Gets the PlayerData instance for a player ('s uuid) from cache
     * or fetch it from the database.
     */
    public PlayerData getPlayerData(UUID playerUUID) {

        // Return from Cache.
        PlayerData playerData = Cache.INSTANCE.getPlayerData(playerUUID);
        if (playerData != null) return playerData;

        // Get from database.
        return PlayerData.get(playerUUID);


    }
    public PlayerData getPlayerData(String playerUUID) {
        return getPlayerData(UUIDUtil.getUUID(playerUUID));
    }


    /*
     * Gets a SkinData instance by the owner's uuid from cache
     * or fetch it from the database.
     */
    public SkinData getSkinData(UUID skinOwnerUUID) {

        // Return from Cache.
        SkinData skinData = Cache.INSTANCE.getSkinData(skinOwnerUUID);
        if (skinData != null) return skinData;

        // Get from database.
        return SkinData.get(skinOwnerUUID);

    }
    public SkinData getSkinData(String skinOwnerName) {
        return getSkinData(UUIDUtil.getUUID(skinOwnerName));
    }


    /*
     * Saves a skin data json string into the database for later usage
     * and returns the uuid which can be used to access it in the future.
     */
    public UUID saveNewSkinData(String profileDataString) {

        // Generate / Populate new skinData.
        UUID skinDataUUID = UUID.randomUUID();

        SkinData skinData = new SkinData();
        skinData.setSkinOwnerUUID(skinDataUUID);
        skinData.setProfileDataString(profileDataString);
        skinData.save();

        return skinDataUUID;

    }


    /*
     * Check & Return whether a skinData exist's in the database or
     * whether a player with that skin exists.
     */
    public static boolean doesSkinDataExist(UUID skinOwnerUUID) {

        // Check database.
        if (DatabaseActions.INSTANCE.getSkinData(skinOwnerUUID) != null) return true;

        // Check Mojang.
        if (UUIDUtil.getName(skinOwnerUUID) != null) return true;

        return false;

    }



    // ================================     SKIN

    /*
     * Changes a players skin into the skin owned by another player.
     * This does not check if the username / uuid is a valid mc one.
     */
    public void changePlayerSkin(Player player, UUID skinOwnerUUID) {

        // Get all requirements.
        PlayerData playerData = getPlayerData(player.getUniqueId());
        SkinData targetSkinData = getSkinData(skinOwnerUUID);

        // Call skin change event.
        PlayerSkinChangeEvent skinChangeEvent = new PlayerSkinChangeEvent(player, targetSkinData);
        Bukkit.getPluginManager().callEvent(skinChangeEvent);

        // Ret: Skin change is cancelled.
        if (skinChangeEvent.isCancelled()) return;

        // Apply the skin.
        targetSkinData.applyToPlayer(player);

        // Change playerData.
        playerData.setCurrentSkinUUID(skinOwnerUUID);
        playerData.save();

    }
    public void changePlayerSkin(Player player, String skinOwnerName) {
        changePlayerSkin(player, UUIDUtil.getUUID(skinOwnerName));
    }


    /*
     * Returns a player head item with a specified target player' skin.
     */
    public ItemStack getSkinHeadItem(UUID skinOwnerUUID) {

        // Get the target skin data.
        SkinData targetSkinData = getSkinData(skinOwnerUUID);

        // Prepare skull item & Return.
        ItemStack skull =  new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
        SkullMeta skullMeta = (SkullMeta) skull.getItemMeta();
        GameProfile profile = GameProfileFactory.INSTANCE.buildProfile(targetSkinData.getProfileDataString());

        try {
            Field profileField = skullMeta.getClass().getDeclaredField("profile");
            profileField.setAccessible(true);
            profileField.set(skullMeta, profile);
        }
        catch (NoSuchFieldException | IllegalAccessException exception) {
            return skull;
        }
        skull.setItemMeta(skullMeta);

        return skull;

    }
    public ItemStack getSkinHeadItem(String skinOwnerName) {
        return getSkinHeadItem(UUIDUtil.getUUID(skinOwnerName));
    }

}
