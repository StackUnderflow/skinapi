package stackunderflow.skinapi;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import stackunderflow.skinapi.api.SkinAPI;
import stackunderflow.skinapi.commands.CommandSkinApi;
import stackunderflow.skinapi.database.DatabaseActions;
import stackunderflow.skinapi.listeners.PlayerJoinListener;
import stackunderflow.skinapi.listeners.PlayerQuitListener;
import stackunderflow.util.commandfactory.CommandFactory;
import stackunderflow.util.config.Config;

import java.sql.SQLException;


/*
 * The main entry & setup point.
 *
 * Events:
 *  - SkinAPIDataDownloadFromMojangEvent (Called when the SkinAPI downloads PlayerData from the Mojang servers.)
 *  - SkinChangeEvent (Called when the SkinAPI changes a players skin.)
 *  - SkinAPI
 *  TODO: Add events.
 *
 * Registered Commands:
 *  - /skinapi
 *
 */
@Getter
@Setter
public class SkinAPIPlugin extends JavaPlugin {

    // ================================     VARS

    // References
    public static SkinAPIPlugin INSTANCE;
    private SkinAPI API;
    private boolean DEBUG = false;


    // Settings
    private String baseCommand = "skin";
    private String pluginChatPrefix = "§7» §6SkinAPI {success}§7*";
    private String version;



    // ================================     CONSTRUCTOR

    public SkinAPIPlugin() {
        if (SkinAPIPlugin.INSTANCE == null) {
            SkinAPIPlugin.INSTANCE = this;
        }
    }



    // ================================     BUKKIT OVERRIDES


    @Override
    public void onEnable() {

        // Setup.
        setVersion(getDescription().getVersion());
        new Config(this);
        new DatabaseActions();
        setPluginChatPrefix(getConfig().getString("plugin.chatPrefix", "§7» §6SkinAPI {success}§7*"));
        SkinAPIPlugin.INSTANCE.DEBUG = getConfig().getBoolean("plugin.DEBUG", false);

        SkinAPIPlugin.INSTANCE.API = new SkinAPI();

        // Register jdbc
        try {
            Class.forName("com.mysql.jdbc.Driver");
        }
        catch (ClassNotFoundException e) {
            System.out.println("ERROR: Could not find Class: com.mysql.jdbc.Driver");
            setEnabled(false);
            return;
        }


        // Establish db connection.
        try {
            DatabaseActions.INSTANCE.openConnection(
                    getConfig().getString("plugin.database.host", "localhost"),
                    Integer.toString(getConfig().getInt("plugin.database.port", 3306)),
                    getConfig().getString("plugin.database.database", "skinapi"),
                    getConfig().getString("plugin.database.username", "skinAPI"),
                    getConfig().getString("plugin.database.password", "skinapi!")
            );
        } catch (SQLException e) {
            setEnabled(false);
            System.out.println("ERROR: Could not open database connection!");
            e.printStackTrace();
            return;
        }


        // Make sure the tables exist.
        try {
            DatabaseActions.INSTANCE.createTables();
        } catch (SQLException e) {
            setEnabled(false);
            System.out.println("ERROR: Could not create database tables!");
            if (this.DEBUG) e.printStackTrace();
            return;
        }


        // Register commands.
        if (getConfig().getBoolean("plugin.enableSetSkinCommand", false)) {
            CommandFactory cmdFac = new CommandFactory();
            cmdFac.addCommand(new CommandSkinApi("skinapi"));
            this.getCommand("skinapi").setExecutor(cmdFac);
        }


        // Register listeners.
        getServer().getPluginManager().registerEvents(new PlayerJoinListener(), this);
        getServer().getPluginManager().registerEvents(new PlayerQuitListener(), this);

    }


    @Override
    public void onDisable() {

        // Close db connection.
        try {
            DatabaseActions.INSTANCE.closeConnection();
        } catch (SQLException e) {
            System.out.println("ERROR: Could not close database connection!");
            e.printStackTrace();
        }

    }



    // ================================     HELPER METHODS


    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args){
        if(cmd.getName().equalsIgnoreCase("skll")){

            ((Player) sender).getInventory().addItem(SkinAPIPlugin.INSTANCE.getAPI().getSkinHeadItem(args[0]));

            /*Player targetPlayer;
            OfflinePlayer skinOwnerPlayer;
            if (args.length > 1) {
                targetPlayer = Bukkit.getPlayer(args[0]);
                skinOwnerPlayer = Bukkit.getOfflinePlayer(args[1]);
            }
            else {
                targetPlayer = (Player) sender;
                skinOwnerPlayer = Bukkit.getOfflinePlayer(args[0]);
            }


            // Ret: !playerExists
            if (targetPlayer == null) return false;

            // Ret: !playerExists
            if (skinOwnerPlayer == null) return false;

            // Change the players skin.
            SkinAPIPlugin.INSTANCE.getAPI().changePlayerSkin(targetPlayer, skinOwnerPlayer.getUniqueId());*/

        }
        return true;
    }


}
