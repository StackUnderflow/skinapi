package stackunderflow.skinapi.playerutils;

import com.google.gson.*;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import com.mojang.authlib.properties.PropertyMap;
import com.mojang.util.UUIDTypeAdapter;
import lombok.Getter;
import lombok.Setter;
import stackunderflow.skinapi.SkinAPIPlugin;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.UUID;


/*
 * The factory can be used to either fetch game profile (basically skin data)
 * from Mojangs servers, or to build a GameProfile out of a preexisting data string.
 */
@Getter
@Setter
public class GameProfileFactory {

    // ================================     VARS

    // References
    public static GameProfileFactory INSTANCE;
    private static Gson gson = new GsonBuilder().disableHtmlEscaping()
            .registerTypeAdapter(UUID.class, new UUIDTypeAdapter())
            .registerTypeAdapter(GameProfile.class, new GameProfileSerializer())
            .registerTypeAdapter(PropertyMap.class, new PropertyMap.Serializer())
            .create();


    // Settings
    private static final String SERVICE_URL = "https://sessionserver.mojang.com/session/minecraft/profile/%s?unsigned=false";
    private static final String JSON_SKIN = "{\"timestamp\":%d,\"profileId\":\"%s\",\"profileName\":\"%s\",\"isPublic\":true,\"textures\":{\"SKIN\":{\"url\":\"%s\"}}}";
    private static final String JSON_CAPE = "{\"timestamp\":%d,\"profileId\":\"%s\",\"profileName\":\"%s\",\"isPublic\":true,\"textures\":{\"SKIN\":{\"url\":\"%s\"},\"CAPE\":{\"url\":\"%s\"}}}";




    // ================================     CONSTRUCTOR

    public GameProfileFactory() {
        GameProfileFactory.INSTANCE = this;
    }



    // ================================     FABRICATION LOGIC

    /*
     * Build & return a GameProfile with the json data String.
     */
    public GameProfile buildProfile(String json) {
        return gson.fromJson(json, GameProfile.class);
    }



    // ================================     FETCHING LOGIC


    /*
     * Fetches the gameProfile data for a user from the Mojang servers.
     * (This returns the JSON String)
     */
    public String fetchData(UUID playerUUID) {

        if (SkinAPIPlugin.INSTANCE.isDEBUG()) System.out.println("[SkinAPI - Debug] Fetching data form mojang servers for: "+playerUUID.toString());

        try {

            // Make HTTP Request.
            HttpURLConnection connection = (HttpURLConnection) new URL(String.format(SERVICE_URL, UUIDTypeAdapter.fromUUID(playerUUID))).openConnection();
            connection.setReadTimeout(6000);

            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {

                // Return gameProfile Json.
                return new BufferedReader(new InputStreamReader(connection.getInputStream())).readLine();

            } else {

                // Convert HTTP Error response into IOException & throw that.
                if (connection.getErrorStream() == null) throw new IOException();
                InputStreamReader isr = new InputStreamReader(connection.getErrorStream());
                BufferedReader bufR = new BufferedReader(isr);
                String s = bufR.readLine();
                if (s == null) throw new IOException();
                JsonObject error = (JsonObject) new JsonParser().parse(s);
                throw new IOException(error.get("error").getAsString() + ": " + error.get("errorMessage").getAsString());

            }

        }
        catch (MalformedURLException muEx) {
            muEx.printStackTrace();
        }
        catch (IOException ioEX) {
            ioEX.printStackTrace();
        }

        return "";

    }



    // ================================     FETCHING LOGIC

    /*
     * A quick and dirty Serializer for gson.
     */
    private static class GameProfileSerializer implements JsonSerializer<GameProfile>, JsonDeserializer<GameProfile> {

        public GameProfile deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
            JsonObject object = (JsonObject) json;
            UUID id = object.has("id") ? (UUID) context.deserialize(object.get("id"), UUID.class) : null;
            String name = object.has("name") ? object.getAsJsonPrimitive("name").getAsString() : null;
            GameProfile profile = new GameProfile(id, name);

            if (object.has("properties")) {
                for (Map.Entry<String, Property> prop : ((PropertyMap) context.deserialize(object.get("properties"), PropertyMap.class)).entries()) {
                    profile.getProperties().put(prop.getKey(), prop.getValue());
                }
            }
            return profile;
        }

        public JsonElement serialize(GameProfile profile, Type type, JsonSerializationContext context) {
            JsonObject result = new JsonObject();
            if (profile.getId() != null)
                result.add("id", context.serialize(profile.getId()));
            if (profile.getName() != null)
                result.addProperty("name", profile.getName());
            if (!profile.getProperties().isEmpty())
                result.add("properties", context.serialize(profile.getProperties()));
            return result;
        }
    }


}
