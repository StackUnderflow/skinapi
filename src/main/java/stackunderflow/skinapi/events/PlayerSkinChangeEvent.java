package stackunderflow.skinapi.events;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import stackunderflow.skinapi.api.SkinData;


/*
 * The event which get's called whenever a players skin should be changed.
 * This contains information about the target player & skin.
 */
@Getter
@Setter
public class PlayerSkinChangeEvent extends Event implements Cancellable {

    // ================================     VARS

    // References
    private Player player;
    private SkinData skinData;

    // State
    private boolean cancelled;
    private static final HandlerList handlers = new HandlerList();




    // ================================     CONSTRUCTOR

    public PlayerSkinChangeEvent(Player player, SkinData skinData) {
        setPlayer(player);
        setSkinData(skinData);
        setCancelled(false);
    }


    @Override
    public boolean isCancelled() {
        return this.cancelled;
    }

    @Override
    public void setCancelled(boolean arg0) {
        this.cancelled = arg0;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
