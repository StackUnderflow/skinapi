package stackunderflow.skinapi.caching;


import lombok.Getter;
import lombok.Setter;
import stackunderflow.skinapi.SkinAPIPlugin;
import stackunderflow.skinapi.api.PlayerData;
import stackunderflow.skinapi.api.SkinData;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


/*
 * Holds a skins data / methods to apply the skin etc.
 */
@Getter
@Setter
public class Cache {

    // ================================     VARS

    // References
    public static Cache INSTANCE;

    private List<SkinData> skinDataCache;
    private List<PlayerData> playerDataCache;

    // Settings
    private int maxCacheSize = 20;



    // ================================     CONSTRUCTOR

    public Cache() {
        Cache.INSTANCE = this;

        setSkinDataCache(new ArrayList<>());
        setPlayerDataCache(new ArrayList<>());
        setMaxCacheSize(SkinAPIPlugin.INSTANCE.getConfig().getInt("plugin.caching.memCacheMaxItems", 20));
    }


    // ================================     CACHE LOGIC


    /*
     * Returns the cached skin data or null if there is none.
     */
    public SkinData getSkinData(UUID skinOwnerUUID) {
        for (SkinData data : getSkinDataCache()) {
            if (data.getSkinOwnerUUID().equals(skinOwnerUUID)) return data;
        }
        return null;
    }


    /*
     * Returns the cached player data or null if there is none.
     */
    public PlayerData getPlayerData(UUID playerUUID) {
        for (PlayerData data : getPlayerDataCache()) {
            if (data.getPlayerUUID().equals(playerUUID)) return data;
        }
        return null;
    }


    /*
     * Add data to the cache.
     */
    public void addSkinData(SkinData data) {
        if ((getSkinDataCache().size()+1) >= getMaxCacheSize()) getSkinDataCache().remove(0);
        if (getSkinData(data.getSkinOwnerUUID()) != null) return;
        getSkinDataCache().add(data);
    }
    public void addPlayerData(PlayerData data) {
        if ((getPlayerDataCache().size()+1) >= getMaxCacheSize()) getPlayerDataCache().remove(0);
        if (getPlayerData(data.getPlayerUUID()) != null) return;
        getPlayerDataCache().add(data);
    }


}

