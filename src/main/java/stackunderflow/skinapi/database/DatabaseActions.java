package stackunderflow.skinapi.database;


import com.mysql.jdbc.Connection;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.codec.binary.Base64;
import stackunderflow.skinapi.api.PlayerData;
import stackunderflow.skinapi.api.SkinData;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;

/*
 * Handle all database calls.
 */
@Getter
@Setter
public class DatabaseActions {

    // ================================     VARS

    // References
    public static DatabaseActions INSTANCE;
    public Connection connection;

    // Settings
    private String playerDataTableName;
    private String skinDataTableName;




    // ================================     CONSTRUCTOR

    public DatabaseActions() {
        DatabaseActions.INSTANCE = this;

        //setupDatabaseConnection();

        //setPlayerDataTableName(Config.INSTANCE.getString("plugin.skinAPI.playerDataTableName", "playerData"));
        //setSkinDataTableName(Config.INSTANCE.getString("plugin.skinAPI.skinDataTableName", "skinData"));

    }



    // ================================     BASIC MANAGER LOGIC


    /*
     * Opens a connection to the specified mysql server.
     */
    public void openConnection(String host, String port, String database, String user, String password) throws SQLException {

        // RET: Connection already open.
        if (this.connection != null && !this.connection.isClosed()) return;

        // Open connection.
        this.connection = (Connection) DriverManager.getConnection("jdbc:mysql://"+host+":"+port+"/"+database, user, password);

    }


    /*
     * Close the database connection.
     */
    public void closeConnection() throws SQLException {
        if (this.connection != null) this.connection.close();
    }


    /*
     * Return whether the connection is open.
     */
    public boolean isConnected() {
        if (this.connection == null) return false;
        try {
            return !this.connection.isClosed();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }


    /*
     * Executes statement and returns data.
     * !! Remember to manually close the statement !!
     */
    public ResultSet excuteQuery(String sql) throws SQLException {

        // RET: No connection.
        if (!this.isConnected()) return null;

        // Execute the statement.
        Statement statement = (Statement) this.connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);

        return resultSet;

    }


    /*
     * Executes an update statement.
     */
    public void executeUpdate(String sql) throws SQLException {

        // RET: No connection.
        if (!this.isConnected()) return;

        // Execute the statement.
        Statement statement = (Statement) this.connection.createStatement();
        statement.executeUpdate(sql);
        statement.close();

    }


    /*
     * Creates the tables for the plugin.
     */
    public void createTables() throws SQLException {

        String s1 = "CREATE TABLE IF NOT EXISTS player_data (playerUUID VARCHAR(36) NOT NULL UNIQUE, userName VARCHAR(16) NOT NULL, currentSkinUUID VARCHAR(36) NOT NULL);";
        String s2 = "CREATE TABLE IF NOT EXISTS skin_data (skinOwnerUUID VARCHAR(36) NOT NULL UNIQUE, profileDataString TEXT NOT NULL);";
        this.executeUpdate(s1);
        this.executeUpdate(s2);

    }




    // ================================     PlayerData & SkinData LOGIC


    /*
     * Returns the PlayerData object from database.
     * Returns null if none exists.
     */
    public PlayerData getPlayerData(UUID uuid) {

        ResultSet resultSet;
        try {
            resultSet = this.excuteQuery("SELECT * FROM player_data WHERE playerUUID=\""+uuid.toString()+"\"");
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

        // RET: No data!
        if (resultSet == null) return null;

        try {

            // RET: No query data found!
            if (!resultSet.next()) return null;

            try {
                PlayerData pData = new PlayerData(
                        UUID.fromString(resultSet.getString("playerUUID")),
                        resultSet.getString("userName"),
                        UUID.fromString(resultSet.getString("currentSkinUUID")));
                pData.save();
                return pData;
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;

    }


    /*
     * Saves the PlayerData into DB or updates existing.
     */
    public void savePlayerData(PlayerData playerData) {

        try {
            this.executeUpdate("INSERT INTO player_data " +
                    "  (playerUUID, userName, currentSkinUUID) " +
                    "VALUES\n" +
                    "  (\""+playerData.getPlayerUUID().toString()+"\", \""+playerData.getUserName()+"\", \""+playerData.getCurrentSkinUUID().toString()+"\") " +
                    "ON DUPLICATE KEY UPDATE " +
                    "  playerUUID     = VALUES(playerUUID), " +
                    "  userName = VALUES(userName), " +
                    "  currentSkinUUID = VALUES(currentSkinUUID)");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    /*
     * Returns the SkinData object from database.
     * Returns null if none exists.
     */
    public SkinData getSkinData(UUID uuid) {

        ResultSet resultSet;
        try {
            resultSet = this.excuteQuery("SELECT * FROM skin_data WHERE skinOwnerUUID=\""+uuid.toString()+"\"");
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

        // RET: No data!
        if (resultSet == null) return null;

        try {

            // RET: No query data found!
            if (!resultSet.next()) return null;

            // Decode profile data.
            byte[] decodedProfileBytes = Base64.decodeBase64(resultSet.getString("profileDataString").getBytes());

            try {
                SkinData sData = new SkinData(
                        UUID.fromString(resultSet.getString("skinOwnerUUID")),
                        new String(decodedProfileBytes));
                sData.save();
                return sData;
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;

    }


    /*
     * Saves the skin data into DB or updates existing.
     */
    public void saveSkinData(SkinData skinData) {

        byte[] encodedProfileBytes = Base64.encodeBase64(skinData.getProfileDataString().getBytes());
        String encodedProfileData = new String(encodedProfileBytes);

        try {
            this.executeUpdate("INSERT INTO skin_data " +
                    "  (skinOwnerUUID, profileDataString) " +
                    "VALUES\n" +
                    "  (\""+skinData.getSkinOwnerUUID().toString()+"\", \""+encodedProfileData+"\") " +
                    "ON DUPLICATE KEY UPDATE " +
                    "  skinOwnerUUID     = VALUES(skinOwnerUUID), " +
                    "  profileDataString = VALUES(profileDataString)");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }




    // ================================     Skins LOGIC

}
