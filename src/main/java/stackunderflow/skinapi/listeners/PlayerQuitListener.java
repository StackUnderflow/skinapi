package stackunderflow.skinapi.listeners;


import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import stackunderflow.skinapi.SkinAPIPlugin;
import stackunderflow.skinapi.api.PlayerData;

/*
 * Reset the players skin on quit.
 */
public class PlayerQuitListener implements Listener {

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {

        // RET: Permission: skinapi.bypass.quitreset
        if (event.getPlayer().hasPermission("skinapi.bypass.quitreset")) return;

        PlayerData playerData = SkinAPIPlugin.INSTANCE.getAPI().getPlayerData(event.getPlayer().getUniqueId());

        // Reset skin.
        SkinAPIPlugin.INSTANCE.getAPI().changePlayerSkin(event.getPlayer(), playerData.getCurrentSkinUUID());
        playerData.setCurrentSkinUUID(event.getPlayer().getUniqueId());
        playerData.save();

    }

}
