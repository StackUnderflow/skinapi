package stackunderflow.skinapi.listeners;


import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import stackunderflow.skinapi.SkinAPIPlugin;
import stackunderflow.skinapi.api.PlayerData;

/*
 * Set's up / loads a players data from the database on join.
 */
public class PlayerJoinListener implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {

        PlayerData playerData = SkinAPIPlugin.INSTANCE.getAPI().getPlayerData(event.getPlayer().getUniqueId());

        // Apply skin if one is set.
        SkinAPIPlugin.INSTANCE.getAPI().changePlayerSkin(event.getPlayer(), playerData.getCurrentSkinUUID());

    }

}
