## About

The SkinAPI can be integrated into any Plugin which needs access to player-skin data.
It also includes some nice helper classes & methods which make skin changing easy.

Take a look inside of the "examples" folder to see an example implementation in a hypothetical plugin.

---

## API Docs


### PlayerData getPlayerData(UUID/String)

Returns the PlayerData object which can be used to get information about specific player data 
like the currently used skin.

This method will ALWAYS return a PlayerData instance. If none exists in the memdb it will search inside of the database 
and finally it will create a new instance for the player & save that to the database,

### SkinData getSkinData(UUID/String)

Returns the SkinData object which can be used to get skin information.

This method will ALWAYS return a SkinData instance. If none exists in the memdb it will search inside of the database 
and finally it will create a new instance for the player & save that to the database.

### UUID saveNewSkinData(String)

Saves custom skin data (a gameprofile json string) into the database.
The returned UUID can be used to access the skin data.

### void changePlayerSkin(Player, UUID/String)

This method changes a Players skin on all Servers with the SkinAPI Plugin installed & configured properly.

### ItemStack getSkinHeadItem(UUID/String)

This method returns a player head item with a specified player's skin.
Due to the method using SkinData objects from the database

---

## Events

#### PlayerSkinChangeEvent

**Properties:**
- Player        - The player whose Skin should be changed.
- SkinData      - The SkinData object which will be used to extract the target skin data.

This event is fired whenever a players skin is changed through the API. It can be canceled just as 
any other event which cancels the skin change & db update.

---

## Commands & Permissions

**/skinapi** - Display information about the plugin.

**Permissions:**
- skinapi.bypass.quitreset | Players skin wont be reset when he disconnects from the server.