package example;

import org.bukkit.plugin.java.JavaPlugin;


/*
 * An example plugin class to demonstrate the SkinAPI implementation.
 * Remember to put the SkinAPI Plugin inside of the servers plugins folder &
 * configure it properly.
 * You also need to include "depends: [SkinAPI]" in the plugin.yml.
 *
 */
public class Plugin extends JavaPlugin {

    // ================================     VARS

    // References
    public static Plugin INSTANCE;



    // ================================     CONSTRUCTOR

    public Plugin() {
        if (Plugin.INSTANCE == null) {
            Plugin.INSTANCE = this;
        }
    }



    // ================================     BUKKIT OVERRIDES


    @Override
    public void onEnable() {

        // ...

        // SkinAPI Setup.
        SkinAPI api = setSkinAPI(new SkinAPI());

        // You can now use the api variable to access all the documented api methods.
        // Recommended: Save the variable as a reference inside the JavaPlugin child class for easy access.

        // ...

    }


    @Override
    public void onDisable() {}


}
